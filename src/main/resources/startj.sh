#!/bin/bash
pid=`ps -ef | grep "easydarwin-jobs-0.0.1-SNAPSHOT.jar" | grep -v grep | awk '{print $2}'`
if [ "$pid" != "" ];then
  echo "already start pid: $pid"
else
  `nohup java -jar easydarwin-jobs-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &`
  pid=`ps -ef | grep "easydarwin-jobs-0.0.1-SNAPSHOT.jar" | grep -v grep | awk '{print $2}'`
  echo $pid
fi

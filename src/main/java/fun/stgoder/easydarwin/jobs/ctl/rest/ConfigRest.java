package fun.stgoder.easydarwin.jobs.ctl.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fun.stgoder.easydarwin.jobs.comm.Code;
import fun.stgoder.easydarwin.jobs.comm.Constants;
import fun.stgoder.easydarwin.jobs.comm.model.Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api("config api")
@RestController
@RequestMapping("/rest/config")
public class ConfigRest {

    @ApiOperation("get ts file expire time (ms)")
    @GetMapping("/ts_expire")
    public Resp getTS_EXPIRE() throws Exception {
        return new Resp(Code.REQUEST_OK, "succ", Constants.TS_EXPIRE);
    }

    @ApiOperation("set ts file expire time (ms)")
    @PostMapping("/ts_expire")
    public Resp setTS_EXPIRE(
            @RequestParam("ts_expire") @ApiParam(name = "ts_expire", value = "ts expire time", required = true) long ts_expire)
            throws Exception {
        Constants.TS_EXPIRE = ts_expire;
        return new Resp(Code.REQUEST_OK, "succ", Constants.TS_EXPIRE);
    }

    @ApiOperation("get record max time (ms)")
    @GetMapping("/record_max_ms")
    public Resp getRECORD_MAX_MS() throws Exception {
        return new Resp(Code.REQUEST_OK, "succ", Constants.RECORD_MAX_MS);
    }

    @ApiOperation("set record max time (ms)")
    @PostMapping("/record_max_ms")
    public Resp setRECORD_MAX_MS(
            @RequestParam("record_max_ms") @ApiParam(name = "ts_expire", value = "record max time", required = true) long record_max_ms)
            throws Exception {
        Constants.RECORD_MAX_MS = record_max_ms;
        return new Resp(Code.REQUEST_OK, "succ", Constants.RECORD_MAX_MS);
    }

    @ApiOperation("get record min time (ms)")
    @GetMapping("/record_min_ms")
    public Resp getRECORD_MIN_MS() throws Exception {
        return new Resp(Code.REQUEST_OK, "succ", Constants.RECORD_MIN_MS);
    }

    @ApiOperation("set record min time (ms)")
    @PostMapping("/record_min_ms")
    public Resp setRECORD_MIN_MS(
            @RequestParam("record_min_ms") @ApiParam(name = "ts_expire", value = "record min time", required = true) long record_min_ms)
            throws Exception {
        Constants.RECORD_MIN_MS = record_min_ms;
        return new Resp(Code.REQUEST_OK, "succ", Constants.RECORD_MIN_MS);
    }

}

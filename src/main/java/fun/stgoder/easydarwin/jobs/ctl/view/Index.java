package fun.stgoder.easydarwin.jobs.ctl.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Index {

    @GetMapping({ "", "/" })
    public ModelAndView index(ModelAndView mv) {

        mv.setViewName("index");
        mv.addObject("title", "index");
        mv.addObject("tab", "index");

        return mv;
    }
}

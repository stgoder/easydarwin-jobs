package fun.stgoder.easydarwin.jobs.ctl.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fun.stgoder.easydarwin.jobs.comm.Constants;
import fun.stgoder.easydarwin.jobs.comm.util.SysUtil;

@Controller
@RequestMapping("/config")
public class Config {

    @GetMapping({"", "/"})
    public ModelAndView config(ModelAndView mv) throws Exception {

        mv.setViewName("config");
        mv.addObject("title", "config");
        mv.addObject("tab", "config");

        mv.addObject("ts_expire", Constants.TS_EXPIRE);
        mv.addObject("record_max_ms", Constants.RECORD_MAX_MS);
        mv.addObject("record_min_ms", Constants.RECORD_MIN_MS);

        mv.addObject("diskUsage", SysUtil.diskUsage(Constants.EASYDARWIN_SERVER_PATH));

        return mv;
    }

    @PostMapping("/ts_expire")
    public ModelAndView setTS_EXPIRE(@RequestParam("ts_expire") long ts_expire, ModelAndView mv) throws Exception {

        mv.setViewName("redirect:/config");

        Constants.TS_EXPIRE = ts_expire;
        return mv;
    }

    @PostMapping("/record_max_ms")
    public ModelAndView setRECORD_MAX_MS(@RequestParam("record_max_ms") long record_max_ms, ModelAndView mv)
            throws Exception {

        mv.setViewName("redirect:/config");

        Constants.RECORD_MAX_MS = record_max_ms;
        return mv;
    }

    @PostMapping("/record_min_ms")
    public ModelAndView setRECORD_MIN_MS(@RequestParam("record_min_ms") long record_min_ms, ModelAndView mv)
            throws Exception {

        mv.setViewName("redirect:/config");

        Constants.RECORD_MIN_MS = record_min_ms;
        return mv;
    }

}

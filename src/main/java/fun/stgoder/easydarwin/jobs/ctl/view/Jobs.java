package fun.stgoder.easydarwin.jobs.ctl.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fun.stgoder.easydarwin.jobs.comm.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import fun.stgoder.easydarwin.jobs.bl.BL;
import fun.stgoder.easydarwin.jobs.bl.entity.Job;
import fun.stgoder.easydarwin.jobs.comm.model.Page;

@Controller
@RequestMapping("/job")
public class Jobs {

    private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @GetMapping("/jobs")
    public ModelAndView jobs(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                             @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize, ModelAndView mv) {

        mv.setViewName("jobs");
        mv.addObject("title", "jobs");
        mv.addObject("tab", "jobs");

        Page<Job> pageData = BL.JOB.getJobs(page, pageSize);
        mv.addObject("pageData", pageData);

        if (page < 1)
            page = 1;

        int totalPages = (int) pageData.getTotal() % pageSize == 0 ? (int) pageData.getTotal() / pageSize
                : ((int) pageData.getTotal() / pageSize) + 1;

        int pageHead = 1;
        int pagePre = page - 1 < 1 ? 1 : page - 1;
        int pageNext = page + 1 > totalPages ? totalPages : page + 1;
        int pageTail = totalPages;

        mv.addObject("pageHead", pageHead);
        mv.addObject("pagePre", pagePre);
        mv.addObject("page", page);
        mv.addObject("pageNext", pageNext);
        mv.addObject("pageTail", pageTail);

        return mv;
    }

    @PostMapping("/add")
    public ModelAndView addJob(@RequestParam("streamId") String streamId, @RequestParam("endTime") String endTime,
                               ModelAndView mv) throws Exception {

        mv.setViewName("redirect:/job/jobs");

        Date dendTime = sdf.parse(endTime);
        long lendTime = dendTime.getTime();

        long lstartTime = System.currentTimeMillis();

        BL.JOB.addJob(streamId, lstartTime, lendTime); // job id

        return mv;
    }

    @PostMapping("/stop")
    public ModelAndView stopJob(@RequestParam("id") String id, ModelAndView mv) throws Exception {

        mv.setViewName("redirect:/job/jobs");

        BL.JOB.stopJobAndMergeFile(id);

        return mv;
    }

    @PostMapping("/delete")
    public ModelAndView deleteJob(@RequestParam("id") String id, ModelAndView mv) throws Exception {

        mv.setViewName("redirect:/job/jobs");

        BL.JOB.deleteJob(id);

        return mv;
    }

    @GetMapping("/play/{id}")
    public ModelAndView playJob(@PathVariable("id") String id, ModelAndView mv) throws Exception {

        mv.setViewName("play-job");

        Job job = BL.JOB.getJob(id);
        if (job == null)
            throw new Exception("job not found");

        String httpReplayPath = "/record/" + job.getStreamId() + "/" + job.getId() + "/index" +
                Constants.RECORD_PLAYLIST_FILE_SUFFIX;

        String httpReplayUrl = "http://" + Constants.localIpv4 + httpReplayPath;

        mv.addObject("httpReplayUrl", httpReplayUrl);
        mv.addObject("jobId", id);

        return mv;
    }

}

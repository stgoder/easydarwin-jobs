package fun.stgoder.easydarwin.jobs;

import fun.stgoder.easydarwin.jobs.comm.Constants;
import fun.stgoder.easydarwin.jobs.comm.util.SqlUtil;
import fun.stgoder.easydarwin.jobs.comm.util.SqlUtil0;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Start {

    private static Logger logger = LoggerFactory.getLogger(Start.class);

    public static void main(String[] args) {

        logger.info("use EASYDARWIN_SERVER_PATH: " + Constants.EASYDARWIN_SERVER_PATH);

        SqlUtil.init();
        SqlUtil0.init();
        SpringApplication.run(Start.class, args);
    }
}
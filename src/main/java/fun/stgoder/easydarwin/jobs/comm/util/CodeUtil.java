package fun.stgoder.easydarwin.jobs.comm.util;

import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CodeUtil {

    private static Logger logger = LoggerFactory.getLogger(CodeUtil.class);

    public static String md5(String text) throws Exception {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(text.getBytes("UTF8"));

        byte[] byteArray = md.digest();

        StringBuffer md5StrBuff = new StringBuffer();

        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }

        return md5StrBuff.toString();
    }

    public static void main(String[] args) throws Exception {
        logger.info(md5("admin"));
    }
}

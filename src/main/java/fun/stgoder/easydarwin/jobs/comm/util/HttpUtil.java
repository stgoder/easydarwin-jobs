package fun.stgoder.easydarwin.jobs.comm.util;

import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import fun.stgoder.easydarwin.jobs.comm.model.NV;

public class HttpUtil {

    // private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    static {
    }

    public static String get(String url) throws Exception {

        Request get = Request.Get(url).connectTimeout(11000).socketTimeout(11000);

        Executor executor = Executor.newInstance();
        HttpResponse response = executor.execute(get).returnResponse();
        int statusCode = response.getStatusLine().getStatusCode();

        String content = IOUtils.toString(response.getEntity().getContent(), Charset.forName("UTF-8"));

        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("req err: " + content);
        } else {
            return content;
        }
    }

    public static String get(String url, NV[] headers) throws Exception {

        Request get = Request.Get(url).connectTimeout(11000).socketTimeout(11000);

        for (NV nv : headers) {
            get.addHeader(nv.getName(), nv.getValue());
        }

        Executor executor = Executor.newInstance();
        HttpResponse response = executor.execute(get).returnResponse();
        int statusCode = response.getStatusLine().getStatusCode();

        String content = IOUtils.toString(response.getEntity().getContent(), Charset.forName("UTF-8"));

        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("req err: " + content);
        } else {
            return content;
        }
    }

    public static String post(String url, String body) throws Exception {

        Request post = Request.Post(url).connectTimeout(11000).socketTimeout(11000).bodyString(body,
                ContentType.DEFAULT_TEXT);

        Executor executor = Executor.newInstance();
        HttpResponse response = executor.execute(post).returnResponse();
        int statusCode = response.getStatusLine().getStatusCode();

        String content = IOUtils.toString(response.getEntity().getContent(), Charset.forName("UTF-8"));

        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("req err: " + content);
        } else {
            return content;
        }

    }

    public static String post(String url, String body, NV[] headers) throws Exception {

        Request post = Request.Post(url).connectTimeout(11000).socketTimeout(11000).bodyString(body,
                ContentType.DEFAULT_TEXT);

        for (NV nv : headers) {
            post.addHeader(nv.getName(), nv.getValue());
        }

        Executor executor = Executor.newInstance();
        HttpResponse response = executor.execute(post).returnResponse();
        int statusCode = response.getStatusLine().getStatusCode();

        String content = IOUtils.toString(response.getEntity().getContent(), Charset.forName("UTF-8"));

        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("req err: " + content);
        } else {
            return content;
        }

    }

}

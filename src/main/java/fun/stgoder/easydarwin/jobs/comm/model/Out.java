package fun.stgoder.easydarwin.jobs.comm.model;

import fun.stgoder.easydarwin.jobs.comm.util.JsonUtil;

public class Out {

    private int exitValue;

    private String output;

    private String errs;

    public Out() {

    }

    public Out(int exitValue, String output, String errs) {
        this.exitValue = exitValue;
        this.output = output;
        this.errs = errs;
    }

    public int getExitValue() {
        return exitValue;
    }

    public void setExitValue(int exitValue) {
        this.exitValue = exitValue;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getErrs() {
        return errs;
    }

    public void setErrs(String errs) {
        this.errs = errs;
    }

    @Override
    public String toString() {
        return JsonUtil.toJson(this);
    }
}

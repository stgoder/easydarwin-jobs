package fun.stgoder.easydarwin.jobs.comm.exception;

public class ExecException extends Exception {
    private int code;
    private String message;

    public ExecException() {
    }

    public ExecException(int code, String message) {
        this.code = code;
        this.message = message;
    }

}

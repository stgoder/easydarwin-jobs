package fun.stgoder.easydarwin.jobs.comm.util;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fun.stgoder.easydarwin.jobs.comm.Constants;

public class SysUtil {

    private static Logger logger = LoggerFactory.getLogger(SysUtil.class);

    private static OS os = null;

    public static enum OS {

        Any("any"), Linux("Linux"), Mac_OS("Mac OS"), Mac_OS_X("Mac OS X"), Windows("Windows"), OS2("OS/2"),
        Solaris("Solaris"), SunOS("SunOS"), MPEiX("MPE/iX"), HP_UX("HP-UX"), AIX("AIX"), OS390("OS/390"),
        FreeBSD("FreeBSD"), Irix("Irix"), Digital_Unix("Digital Unix"), NetWare_411("NetWare"), OSF1("OSF1"),
        OpenVMS("OpenVMS"), Others("Others");

        private OS(String os) {
            this.os = os;
        }

        private String os;

        @Override
        public String toString() {
            return os;
        }
    }

    public static OS os() {

        if (SysUtil.os != null)
            return SysUtil.os;

        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("linux") >= 0) {
            SysUtil.os = OS.Linux;
            return OS.Linux;
        }
        if (os.indexOf("mac") >= 0 && os.indexOf("os") > 0 && os.indexOf("x") < 0) {
            SysUtil.os = OS.Mac_OS;
            return OS.Mac_OS;
        }
        if (os.indexOf("mac") >= 0 && os.indexOf("os") > 0 && os.indexOf("x") > 0) {
            SysUtil.os = OS.Mac_OS_X;
            return OS.Mac_OS_X;
        }
        if (os.indexOf("windows") >= 0) {
            SysUtil.os = OS.Windows;
            return OS.Windows;
        }
        if (os.indexOf("os/2") >= 0) {
            SysUtil.os = OS.OS2;
            return OS.OS2;
        }
        if (os.indexOf("solaris") >= 0) {
            SysUtil.os = OS.Solaris;
            return OS.Solaris;
        }
        if (os.indexOf("sunos") >= 0) {
            SysUtil.os = OS.SunOS;
            return OS.SunOS;
        }
        if (os.indexOf("mpe/ix") >= 0) {
            SysUtil.os = OS.MPEiX;
            return OS.MPEiX;
        }
        if (os.indexOf("hp-ux") >= 0) {
            SysUtil.os = OS.HP_UX;
            return OS.HP_UX;
        }
        if (os.indexOf("aix") >= 0) {
            SysUtil.os = OS.AIX;
            return OS.AIX;
        }
        if (os.indexOf("os/390") >= 0) {
            SysUtil.os = OS.OS390;
            return OS.OS390;
        }
        if (os.indexOf("freebsd") >= 0) {
            SysUtil.os = OS.FreeBSD;
            return OS.FreeBSD;
        }
        if (os.indexOf("irix") >= 0) {
            SysUtil.os = OS.Irix;
            return OS.Irix;
        }
        if (os.indexOf("digital") >= 0 && os.indexOf("unix") > 0) {
            SysUtil.os = OS.Digital_Unix;
            return OS.Digital_Unix;
        }
        if (os.indexOf("netware") >= 0) {
            SysUtil.os = OS.NetWare_411;
            return OS.NetWare_411;
        }
        if (os.indexOf("osf1") >= 0) {
            SysUtil.os = OS.OSF1;
            return OS.OSF1;
        }
        if (os.indexOf("openvms") >= 0) {
            SysUtil.os = OS.OpenVMS;
            return OS.OpenVMS;
        }

        return OS.Any;
    }

    public static String diskUsage(String path) throws Exception {

        String usage = "unknown os";

        if (os() == OS.Windows) {
            File file = new File("c:");
            long totalSpace = file.getTotalSpace();
            long freeSpace = file.getFreeSpace();
            long usedSpace = totalSpace - freeSpace;

            usage = "all: " + totalSpace / 1024 / 1024 / 1024 + "G" + "\n" + "free space: "
                    + freeSpace / 1024 / 1024 / 1024 + "G" + "\n" + "used: " + usedSpace / 1024 / 1024 / 1024 + "G"
                    + "\n";
        }
        if (os() == OS.Linux) {
            usage = ProcessUtil.exec("df -h").getOutput();
        }

        if (Constants.TEST_MODE)
            logger.info(usage);
        return usage;
    }

    public static void main(String[] args) throws Exception {
        diskUsage(Constants.EASYDARWIN_SERVER_PATH);
    }
}

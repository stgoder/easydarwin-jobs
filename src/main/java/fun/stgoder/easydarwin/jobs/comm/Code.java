package fun.stgoder.easydarwin.jobs.comm;

public class Code {

    public static final int SERVICE_LOCKED = -1;

    public static final int REQUEST_OK = 200;

    public static final int REQUEST_ERR = 500;

    public static final int ERR_PARAM = 510;

    public static final int ERR_FILE_NOT_FOUND = 530;
    public static final int ERR_FILE_NO_PERMISSION = 530;

    public static final int USER_LOGIN_NOT_EXIST = 3001;

    public static final int USER_LOGIN_ERR_PASSWORD = 3002;

    public static final int PERMISSION_NEED_LOGIN = 5001;

    public static final int PERMISSION_TOKEN_MISSING = 5010;
    public static final int PERMISSION_TOKEN_EXPIRE = 5011;
    public static final int PERMISSION_TOKEN_USER_NOT_EXIST_IN_DB = 5012;
    public static final int PERMISSION_TOKEN_NO_RIGHT = 5013;

    private Code() {
    }
}

package fun.stgoder.easydarwin.jobs.comm;

import fun.stgoder.easydarwin.jobs.comm.util.SysUtil;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.util.Map;

public class Constants {
    public static final String EASYDARWIN_SERVER_PATH;
    public static final String EASYDARWIN_M3U8_PATH;
    public static final String EASYDARWIN_JOBS_DB_PATH;
    public static final String EASYDARWIN_DB_PATH;
    public static final String EASYDARWIN_API_BASE = "http://localhost:10008";
    public static final String FFMPEG_PATH;
    public static final String FFPROBE_PATH;
    public static long TS_EXPIRE;
    public static long RECORD_MAX_MS;
    public static long RECORD_MIN_MS;
    public static final int TS_DURATION_SECOND;
    public static final String RECORD_FILE_SUFFIX = ".ts";
    public static final String RECORD_PLAYLIST_FILE_SUFFIX = ".m3u8";
    public static final boolean PRE_RECORD;
    public static final int MIN_PRE_RECORD_SECOND;
    public static boolean TEST_MODE = true;
    public static final String localIpv4;
    public static final boolean COVERT_RECORD_TO_MP4;
    public static final String PSLOG_PATH;
    public static final String RECORD_DIR;

    static {
        Yaml yaml = new Yaml();
        Map base = yaml.loadAs(Constants.class.getResourceAsStream("/application.yml"), Map.class);
        Map myConfig = (Map) base.get("myConfig");

        TEST_MODE = (boolean) myConfig.getOrDefault("test-mode", false);
        localIpv4 = (String) myConfig.getOrDefault("local-ipv4", "127.0.0.1");

        String userDir = System.getProperty("user.dir");
        File iniFile = new File(userDir + File.separator + "easydarwin.ini");
        if (iniFile.exists()) {
            EASYDARWIN_SERVER_PATH = userDir;
        } else {
            EASYDARWIN_SERVER_PATH = (String) myConfig.get("easydarwin-server-path");
        }
        EASYDARWIN_M3U8_PATH = EASYDARWIN_SERVER_PATH + File.separator + "m3u8";
        EASYDARWIN_DB_PATH = EASYDARWIN_SERVER_PATH + File.separator + "easydarwin.db";
        EASYDARWIN_JOBS_DB_PATH = EASYDARWIN_SERVER_PATH + File.separator + "easydarwin-jobs.db";
        if (SysUtil.os() == SysUtil.OS.Windows) {
            FFMPEG_PATH = EASYDARWIN_SERVER_PATH + File.separator + "ffmpeg.exe";
            FFPROBE_PATH = EASYDARWIN_SERVER_PATH + File.separator + "ffprobe.exe";
        } else {
            FFMPEG_PATH = EASYDARWIN_SERVER_PATH + File.separator + "ffmpeg";
            FFPROBE_PATH = EASYDARWIN_SERVER_PATH + File.separator + "ffprobe";
        }
        PSLOG_PATH = EASYDARWIN_SERVER_PATH + File.separator + "pslog";
        File pslogDir = new File(PSLOG_PATH);
        if (!pslogDir.exists())
            pslogDir.mkdirs();

        // media-path
        TS_EXPIRE = (long) (int) myConfig.getOrDefault("ts-expire", 40 * 60 * 1000);
        RECORD_MAX_MS = (long) (int) myConfig.getOrDefault("record-max-ms", 35 * 60 * 1000);
        RECORD_MIN_MS = (long) (int) myConfig.getOrDefault("record-min-ms", 1 * 60 * 1000);
        TS_DURATION_SECOND = (int) myConfig.getOrDefault("ts-duration-second", 30);
        PRE_RECORD = (boolean) myConfig.getOrDefault("pre-record", true);
        MIN_PRE_RECORD_SECOND = (int) myConfig.getOrDefault("min-pre-record-second", 10);
        COVERT_RECORD_TO_MP4 = (boolean) myConfig.getOrDefault("convert-record-to-mp4", false);
        RECORD_DIR = (String) myConfig.getOrDefault("record-dir", "/home/stgoder/record");
        File recordDir = new File(RECORD_DIR);
        if (!recordDir.exists())
            recordDir.mkdirs();
    }

    private Constants() {
    }

}

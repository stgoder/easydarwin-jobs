package fun.stgoder.easydarwin.jobs.bl.model.api;

public class Player {

    private String id;

    private String path;

    private String transType;

    private long inBytes;

    private long outBytes;

    private String startAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public long getInBytes() {
        return inBytes;
    }

    public void setInBytes(long inBytes) {
        this.inBytes = inBytes;
    }

    public long getOutBytes() {
        return outBytes;
    }

    public void setOutBytes(long outBytes) {
        this.outBytes = outBytes;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

}

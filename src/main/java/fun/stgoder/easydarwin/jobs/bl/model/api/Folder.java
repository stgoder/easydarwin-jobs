package fun.stgoder.easydarwin.jobs.bl.model.api;

public class Folder {

    private String folder;

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

}

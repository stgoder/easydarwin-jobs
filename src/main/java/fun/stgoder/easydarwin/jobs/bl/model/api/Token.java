package fun.stgoder.easydarwin.jobs.bl.model.api;

public class Token {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}

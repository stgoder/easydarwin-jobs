package fun.stgoder.easydarwin.jobs.bl.entity;

import fun.stgoder.easydarwin.jobs.comm.util.JsonUtil;

public class Job {

    public static final String BASE_COLS = "id, type, lstartTime, lendTime, status, streamId, sts, ets, realDuration, length";

    private String id;

    private int type; // 1.record

    private long lstartTime;

    private long lendTime;

    private int status; // 0.waiting 1.making 2.complete -1.failed

    private String streamId;

    private long sts;

    private long ets;

    private float realDuration; //s

    private long length;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getLstartTime() {
        return lstartTime;
    }

    public void setLstartTime(long lstartTime) {
        this.lstartTime = lstartTime;
    }

    public long getLendTime() {
        return lendTime;
    }

    public void setLendTime(long lendTime) {
        this.lendTime = lendTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public long getSts() {
        return sts;
    }

    public void setSts(long sts) {
        this.sts = sts;
    }

    public long getEts() {
        return ets;
    }

    public void setEts(long ets) {
        this.ets = ets;
    }

    public float getRealDuration() {
        return realDuration;
    }

    public void setRealDuration(float realDuration) {
        this.realDuration = realDuration;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return JsonUtil.toJson(this);
    }

}

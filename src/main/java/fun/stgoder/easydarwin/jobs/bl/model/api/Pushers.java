package fun.stgoder.easydarwin.jobs.bl.model.api;

import java.util.List;

public class Pushers {

    private int total;

    private List<Pusher> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Pusher> getRows() {
        return rows;
    }

    public void setRows(List<Pusher> rows) {
        this.rows = rows;
    }

}

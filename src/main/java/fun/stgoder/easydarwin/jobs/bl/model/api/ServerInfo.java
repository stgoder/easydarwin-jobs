package fun.stgoder.easydarwin.jobs.bl.model.api;

public class ServerInfo {

    private String Hardware;

    private String RunningTime;

    private String StartUpTime;

    private String Server;

    public String getHardware() {
        return Hardware;
    }

    public void setHardware(String hardware) {
        Hardware = hardware;
    }

    public String getRunningTime() {
        return RunningTime;
    }

    public void setRunningTime(String runningTime) {
        RunningTime = runningTime;
    }

    public String getStartUpTime() {
        return StartUpTime;
    }

    public void setStartUpTime(String startUpTime) {
        StartUpTime = startUpTime;
    }

    public String getServer() {
        return Server;
    }

    public void setServer(String server) {
        Server = server;
    }

}

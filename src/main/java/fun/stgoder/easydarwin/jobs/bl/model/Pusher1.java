package fun.stgoder.easydarwin.jobs.bl.model;

public class Pusher1 {
    private String streamId;
    private String rtsp;

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getRtsp() {
        return rtsp;
    }

    public void setRtsp(String rtsp) {
        this.rtsp = rtsp;
    }
}

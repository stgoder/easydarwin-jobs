package fun.stgoder.easydarwin.jobs.bl.model.api;

import java.util.List;

public class Folders {

    private int total;

    private List<Folder> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Folder> getRows() {
        return rows;
    }

    public void setRows(List<Folder> rows) {
        this.rows = rows;
    }

}

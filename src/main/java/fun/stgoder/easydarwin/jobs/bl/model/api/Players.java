package fun.stgoder.easydarwin.jobs.bl.model.api;

import java.util.List;

public class Players {

    private int total;

    private List<Player> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Player> getRows() {
        return rows;
    }

    public void setRows(List<Player> rows) {
        this.rows = rows;
    }

}

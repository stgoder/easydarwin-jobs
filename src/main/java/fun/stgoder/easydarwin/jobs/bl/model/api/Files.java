package fun.stgoder.easydarwin.jobs.bl.model.api;

import java.util.List;

public class Files {

    private int total;

    private List<File> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<File> getRows() {
        return rows;
    }

    public void setRows(List<File> rows) {
        this.rows = rows;
    }

}
